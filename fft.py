from myhdl import *


@block
def fft(clk, reset, konstant_factor, out_value):
    state_t = enum('WRITE', 'READ', 'RETU')
    state = Signal(state_t.READ)

    # out_value = Signal(intbv(0, min=-2 * 50, max=2 * 50))
    internal_width = len(out_value) - 2
    Reg_T0 = Signal(intbv(2**4, min=out_value.min, max=out_value.max))
    Reg_T1 = Signal(intbv(0, min=out_value.min, max=out_value.max))
    counter = Signal(intbv(0, min=-1024, max=1024))
    fft_value = [Signal(intbv(0, min=out_value.min, max=out_value.max)) for i in range(8)]
    fft_even = [Signal(intbv(0, min=out_value.min, max=out_value.max)) for i in range(4)]
    fft_odd = [Signal(intbv(0, min=out_value.min, max=out_value.max)) for i in range(4)]

    suma = Signal(intbv(0, min=-2048, max=2048))

    for i in range(8):
        fft_value[i].driven = True

    for i in range(4):
        fft_even[i].driven = True

    for i in range(4):
        fft_odd[i].driven = True

    @always_seq(clk.posedge, reset=reset)
    def sum_proc():
        if state == state_t.READ:
            Reg_T0.next = Reg_T1
            Reg_T1.next = ((konstant_factor * Reg_T1) >> (internal_width - 1)) - Reg_T0
            state.next = state_t.WRITE
        elif state == state_t.WRITE:
            suma.next = suma * fft_value[counter]
            counter.next = counter + 1
            state.next = state_t.WRITE
            if counter == 7:
                state.next = state_t.RETU
                counter.next = 0
        elif state == state_t.RETU:
            out_value.next = Reg_T1
            state.next = state_t.READ
        else:
            raise ValueError("Undefined state")

    return instances()
