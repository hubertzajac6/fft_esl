import os

from myhdl import *
from math import cos, pi

from ClkDriver import ClkDriver
from fft import fft
from lib.hw.axi.axis import Axis
from lib.hw.myhdl_sim.clk_stim import clk_stim


@block
def test_fft(vhdl_output_path=None):
    clk = Signal(bool(0))
    reset = ResetSignal(1, active=1, async=False)
    out_value = Signal(intbv(0)[16:].signed())
    internal_width = len(out_value) - 2
    freq_sin = 0.5e6
    clk_freq = 10e6
    konstant_factor = Signal(intbv(0)[len(out_value):])

    period = 10

    low_time = int(period / 2)
    high_time = period - low_time

    @instance
    def drive_clk():
        while True:
            yield delay(low_time)
            clk.next = 1
            yield delay(high_time)
            clk.next = 0

    @instance
    def reset_gen():
        reset.next = 1
        yield delay(200)
        yield clk.negedge
        konstant_factor.next = int(cos(2 * pi * freq_sin * 1.0 / clk_freq) * 2 ** (internal_width))
        for i in range(20):
            yield clk.negedge
        reset.next = 0
        StopSimulation()

    fft_instance = fft(clk, reset, konstant_factor, out_value)

    if vhdl_output_path is not None:
        fft_instance.convert(hdl='VHDL', path=vhdl_output_path, initial_values=True)

    return instances()


if __name__ == '__main__':
    trace_save_path = f'{os.curdir}/out/testbench/'
    vhdl_output_path = f'{os.curdir}/out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb = test_fft(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='test_fft')
    tb.run_sim()
